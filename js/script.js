function shuffle(names) {
    var length = names.length;
    if (length == 0) {
        errorDisplay("Hmmmmmm!", "Comment names are not found!");
    }
    var name, link;
    $(".nice-text-display").fadeIn("slow");
    set_interval(function () {
        var random = names[(Math.random() * names.length) | 0];
        var name = random.split("--urlhere--")[0];
        var url = random.split("--urlhere--")[1];
        $("#chosen_name").text(name);
        $("#chosen_name_url").attr("href", url);
    }, Math.ceil(Math.log(names.length)) * 2, 200);
}
/**
 * This function gives a counter to the window.setInterval() function by canceling the interval after a certain amount of calls.
 * @param {*function} callback the logic function goes here
 * @param {*int} count how many times the function is to be repeated
 * @param {*int} timeout how long is the function repeated for
 */
function set_interval(callback, count, timeout) {
    var x = 0;
    var interval = window.setInterval(function () {
        callback();
        if (++x == count) {
            window.clearInterval(interval);
        }
    }, timeout);
}
function getRandomCommentNames(match) {
    try {
        var videoId = match[1];
        getNames(videoId);
    } catch (e) {
        alert("UH Something went wrong. Please make sure the url is valid. Thank you.");
        return;
    }

}
function getNames(videoId) {
    var commenterName = new Set();
    try {
        $.ajax({
            url: "https://www.googleapis.com/youtube/v3/commentThreads?videoId=" + videoId + "&part=snippet&key=AIzaSyAmjaES2rZFzgKFAgJuzs9wf0rAPrCZgzc&maxResults=100",
            type: "GET",
            dataType: 'json',
            success: function (data) {
                for (var i = 0; i < data.items.length; i++) {
                    var temp_name = data.items[i].snippet.topLevelComment.snippet.authorDisplayName + "--urlhere--" + data.items[i].snippet.topLevelComment.snippet.authorChannelUrl;
                    commenterName.add(temp_name);
                }
                shuffle(Array.from(commenterName));
                return;
            }
        });
    } catch (e) {
        errorDisplay("No Comments", "Comment names cannot be fetched. Try different url");
        $("#youtubeURL").val("");
        return;
    }
}

function validateUrl() {
    try {
        var url = $("#youtubeURL").val();
        // console.log(url);
        var regex = /(?:[?&]v=|\/embed\/|\/1\/|\/v\/|\/?youtu\.be\/)([^&\n?#]+)/g;
        var match = regex.exec(url);
        if (match == null || match.length < 2) {
            $("#youtubeURL").addClass("alert alert-danger");
            return;
        }
        $("#youtubeURL").removeClass("alert alert-danger");
        getNames(match[1]);
    } catch (e) {
        errorDisplay("Ooppss!!", "Something went wrong! Make sure the url is valid.");
        return;
    }
}

function errorDisplay(header, body) {
    $("#alert_header").val(header);
    $("#alert_text").val(body);
    $("#alert_modal").modal("show");
}

/**
 * Centers a did in relation to the parent div
 * @param {*} outerDiv The parent div for center position
 * @param {*} innerDiv The child div that is being centered
 * @param {*} extraDiv The extra parameter that is being taken care of Default null
 * @param {*} extraPadding removes extra padding if any available
 */
function centerDiv(outerDiv, innerDiv, extraDiv = null, removeExtra = 0) {
    var new_top_padding;
    if (extraDiv == null) {
        new_top_padding = (outerDiv.height() - innerDiv.height() - removeExtra) / 2;
    } else {
        new_top_padding = (outerDiv.height() - innerDiv.height() - extraDiv.height() - removeExtra) / 2;
    }
    innerDiv.css("margin-top", new_top_padding + "px");
}

$(window).ready(function () {

});

$(window).resize(function () {
    // centerDiv($(document), $(".middle"), $("nav")); // center url-input-txt
});

